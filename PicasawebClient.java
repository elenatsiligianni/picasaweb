
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gdata.client.photos.PicasawebService;
import com.google.gdata.data.Link;
import com.google.gdata.data.photos.AlbumEntry;
import com.google.gdata.data.photos.AlbumFeed;
import com.google.gdata.data.photos.CommentEntry;
import com.google.gdata.data.photos.GphotoEntry;
import com.google.gdata.data.photos.GphotoFeed;
import com.google.gdata.data.photos.PhotoEntry;
import com.google.gdata.data.photos.TagEntry;
import com.google.gdata.data.photos.UserFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

/**
 * This is a simple client that provides high-level operations on the Picasa Web
 * Albums GData API.
 * 
 */
public class PicasawebClient {

	private static final String API_PREFIX = "http://picasaweb.google.com/data/feed/api/user/";

	private final PicasawebService service;

	/**
	 * Constructs a new un-authenticated client.
	 */
	public PicasawebClient(PicasawebService service) {
		this(service, null, null);
	}

	/**
	 * Constructs a new client with the given username and password.
	 */
	public PicasawebClient(PicasawebService service, String uname, String passwd) {
		this.service = service;
		if (uname != null && passwd != null) {
			try {
				service.setUserCredentials(uname, passwd);
			} catch (AuthenticationException e) {
				throw new IllegalArgumentException("Illegal username/password combination.");
			}
		}
	}


}
