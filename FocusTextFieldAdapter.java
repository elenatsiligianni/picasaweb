
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.text.JTextComponent;


public class FocusTextFieldAdapter extends FocusAdapter {

	@Override
	public void focusGained(FocusEvent e) {
		if (e.getComponent() instanceof JTextComponent) {
			JTextComponent tb = (JTextComponent) e.getComponent();
			tb.selectAll();
		}
	}
	
}
