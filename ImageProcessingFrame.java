import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;


import com.google.gdata.client.photos.PicasawebService;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.media.MediaFileSource;
import com.google.gdata.data.photos.AlbumEntry;
import com.google.gdata.data.photos.PhotoEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.google.gdata.client.Service;
import com.google.gdata.client.*;
import com.google.gdata.client.http.AuthSubUtil;
import com.google.gdata.client.photos.*;
import com.google.gdata.data.*;
import com.google.gdata.data.media.*;
import com.google.gdata.data.photos.*;
import java.util.List;



/**
 * This frame has a menu to load an image and to specify various transformations, and a component to
 * show the resulting image.
 */
public class ImageProcessingFrame extends JFrame  {


       private BufferedImage image;
       private static final int DEFAULT_WIDTH = 800;
       private static final int DEFAULT_HEIGHT = 800;

        private PicasawebService service;
        private PicasawebClient client;
	private JPanel loginPanel;
	private JTextField usernameTextBox;
	private JPasswordField passwordTextBox;
	private JButton loginButton;


        boolean loginstate = false;      //simaies pou tha xreiastw........
        boolean there_is_image = false;

        private JFrame controllingFrame; //needed for dialogs

        //gia send
        public int index;
        JComboBox combo;
        String str;
        URL albumPostUrl;
        PhotoEntry myPhoto;
        ArrayList<String> id ;
        char charArray[] = new char[ 19 ];

        //experimentals
       private String title;
        private String description;
        private String url;
        private String filename;
        private AlbumEntry album;
        private PhotoEntry entry;
        private PhotoEntry createdEntry;
        private PhotoEntry photoEntry;
        private String desc;

        public String path;
        public String imageInfo;


       
    

    public static void main(String[] args)
    {
      EventQueue.invokeLater(new Runnable()
         {
            public void run()
            {

            JFrame prframe = new ImageProcessingFrame();
            

            prframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            prframe.setVisible(true);   
            }
         });

     }//telos tis main




   public ImageProcessingFrame()
   {


      setTitle("ImageProcessing");
      setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

      getContentPane().add(getLoginPanel(), BorderLayout.SOUTH);

      this.service = new PicasawebService("PicasawebClient");
      this.client = null;



      add(new JComponent()
         {
            @Override
            public void paintComponent(Graphics g)
            {
               if (image != null) g.drawImage(image, 0, 0, null);

      
            }
         });


//Build File Menu
      JMenu fileMenu = new JMenu("File");

      //File->Open
      JMenuItem openItem = new JMenuItem("Open");
      openItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
               openFile();
            }
         });
      openItem.setMnemonic('o');
      openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
      fileMenu.add(openItem);


      //File->Save
      JMenuItem saveItem = new JMenuItem("Save");
      saveItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
               saveFile();

            }

            private void saveFile() {
            JFileChooser chooser = new JFileChooser();
            int returnVal = chooser.showSaveDialog(null);
            File file = chooser.getSelectedFile();


            path = ("C:/ntua.jpg "); 
     String path = file.getAbsolutePath();

      if (path == null )     {

      System.out.println("null pointer!!!!!!!");
       }


            System.out.println("Path="+path);

            JAI.create("filestore", image, path, "JPEG");
  //           String path = file.getAbsolutePath();


            }
         });
      saveItem.setMnemonic('s');
      saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
      fileMenu.add(saveItem);

      //File->Print

      JMenuItem printItem = new JMenuItem("Print");
      printItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
               printFile(event);
            }
      });
      printItem.setMnemonic('p');
      printItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
      fileMenu.add(printItem);

      //File->Exit

      JMenuItem exitItem = new JMenuItem("Exit");
      exitItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
               System.exit(0);
            }
         });

      fileMenu.add(exitItem);


//Build About

       JMenu aboutMenu = new JMenu("About");
       //About->About
       JMenuItem aboutItem = new JMenuItem("About");

       aboutItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
                //Not yet impemented!!!!
                System.out.println("Sorry nothing here!!!");
            }
       });

       aboutMenu.add(aboutItem);
       

//Build Tools

      JMenu ToolsMenu = new JMenu("Tools");
      

          //Tools->Scale

      JMenuItem scaleItem = new JMenuItem("Scale");
      scaleItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
                if (image == null) return;


     AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(0.5, 0.5), null);

               filter(op);

            }
        });

      ToolsMenu.add(scaleItem);


     //Tools->Edge Detect

      JMenuItem edgeDetectItem = new JMenuItem("Edge detect");
      edgeDetectItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
               float[] elements = { 0.0f, -1.0f, 0.0f, -1.0f, 4.f, -1.0f, 0.0f, -1.0f, 0.0f };
               convolve(elements);
            }
         });
      ToolsMenu.add(edgeDetectItem);


     //Tools->Rotate

      JMenuItem rotateItem = new JMenuItem("Rotate");
      rotateItem.addActionListener(new ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
               if (image == null) return;
               AffineTransform transform = AffineTransform.getRotateInstance(Math.toRadians(5),
                     image.getWidth() / 2, image.getHeight() / 2);
               AffineTransformOp op = new AffineTransformOp(transform,
                     AffineTransformOp.TYPE_BICUBIC);
              filter(op);
            }
         });
      ToolsMenu.add(rotateItem);


      JButton picasa = new JButton("Picasa service");//dimiourgia tou button


      //Add action listener to button
        picasa.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e)
            {
                  
      if  ((loginstate == false) && (there_is_image == true))   {
          System.out.println("Den eisai connected kai yparxei eikona");

                   //Prepei na kaneis login

         JFrame controllingFrame  =   new JFrame(); //To frame tou dialogou

            JOptionPane.showMessageDialog(controllingFrame,
                    "You are not connected! Please log in",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);
          }
      
          if  ((loginstate == false) && (there_is_image == false))   {
          System.out.println("Den eisai connected kai den yparxei eikona");
                   //Prepei na kaneis login

                 JFrame controllingFrame  =   new JFrame(); //To frame tou dialogou

            JOptionPane.showMessageDialog(controllingFrame,
                    "You are not connected! Please log in",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);

          }      

           if ((loginstate==true)&&(there_is_image == false)) {
          System.out.println("Eisai connected kai den yparxei eikona");
                    //Anoikse eikona

                     JFrame controllingFrame  =   new JFrame(); //To frame tou dialogou

            JOptionPane.showMessageDialog(controllingFrame,
                    "There is no image right now.Please load an image",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);

            }
                
       if ( (loginstate == true) && (there_is_image == true) ) 
       
       {
            //Execute when button is pressed

            try {
                SendImage(); 
            } catch (AuthenticationException ex) {
                Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServiceException ex) {
                Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
                   

           }//to if


                System.out.println("You clicked picasa service button");

            } //telos actionperformed
        });

  

      JMenuBar menuBar = new JMenuBar();
    
      menuBar.add(fileMenu);
      menuBar.add(ToolsMenu);
      menuBar.add(aboutMenu);

      menuBar.add(picasa);//prosthiki sti menuBar tou button
      
     
      setJMenuBar(menuBar);
   }


   /**
    * Open a file and load the image.
    */
   public void openFile()
   {
      JFileChooser chooser = new JFileChooser();
      chooser.setCurrentDirectory(new File("."));
      String[] extensions = ImageIO.getReaderFileSuffixes();
      chooser.setFileFilter(new FileNameExtensionFilter("Image files", extensions));
      int r = chooser.showOpenDialog(this);
     

      if (r != JFileChooser.APPROVE_OPTION) return;

      try
      {
 
         Image img = ImageIO.read(chooser.getSelectedFile());
         image = new BufferedImage(img.getWidth(null), img.getHeight(null),
         BufferedImage.TYPE_INT_RGB);
         image.getGraphics().drawImage(img, 0, 0, null);  

         String imageInfo = "Dimensions: " + image.getWidth() + "x" + image.getHeight();
         System.out.println(imageInfo);


         there_is_image = true; //Yparxei anoixti eikona

  //        String path= chooser.getSelectedFile().getPath();

      //   compos = null;

      }
      catch (IOException e)
      {
         JOptionPane.showMessageDialog(this, e);
      }
      repaint();
    

      System.out.println("Don't forget to put the suffix .jpeg or another of image format when save!!!");
   }

   
//Print
   private void printFile(ActionEvent event) {
       PrinterJob printer = PrinterJob.getPrinterJob();
        //printer.setPrintable( this);
        HashPrintRequestAttributeSet printAttr = new HashPrintRequestAttributeSet();
        if(printer.printDialog(printAttr))     // Display print dialog
        {            // If true is returned...
            try
            {
                printer.print(printAttr);    // then print
            }
            catch(PrinterException e)
            {
                JOptionPane.showMessageDialog(this,"Failed to print the file: "+e,"Error",JOptionPane.ERROR_MESSAGE);
            }
        }
   }


   /**
    * Apply a filter and repaint.
    * @param op the image operation to apply
    */
   private void filter(BufferedImageOp op)
   {
      if (image == null) return;
      image = op.filter(image, null);
      repaint();
   }

   /**
    * Apply a convolution and repaint.
    * @param elements the convolution kernel (an array of 9 matrix elements)
    */
   private void convolve(float[] elements)
   {
      Kernel kernel = new Kernel(3, 3, elements);
      ConvolveOp op = new ConvolveOp(kernel);
      filter(op);
   }

   private JPanel getLoginPanel()
      {
		if (loginPanel == null)
            {
                loginPanel = new JPanel(new GridLayout(2, 1, 2, 2));
                loginPanel.setBorder(BorderFactory.createTitledBorder("Login"));

                JPanel pnl = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 1));
                JLabel lbl = new JLabel("Username: ");
                lbl.setPreferredSize(new Dimension(75, 16));
                pnl.add(lbl);
                pnl.add(getUsernameTextField());
                pnl.add(Box.createHorizontalGlue());
                loginPanel.add(pnl);

                pnl = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 1));
                lbl = new JLabel("Password: ");
                lbl.setPreferredSize(new Dimension(75, 16));
                pnl.add(lbl);
                pnl.add(getPasswordTextField());
                pnl.add(getLoginButton());
                pnl.add(Box.createHorizontalGlue());
                loginPanel.add(pnl);


                KeyAdapter adapter = new KeyAdapter()
                      {
                        public void keyPressed(KeyEvent e)
                               {
                                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                                   getLoginButton().doClick();
				}
		      };
			getUsernameTextField().addKeyListener(adapter);
			getPasswordTextField().addKeyListener(adapter);

	    }
		return loginPanel;
        }


   private JButton getLoginButton()
        {
		if (loginButton == null)
                {
                    loginButton = new JButton(new AbstractAction(" Login ")
                       {
                    public void actionPerformed(ActionEvent e)
                              {
                            login();
                               }
			});
		}
	     return loginButton;
	}

	private JTextField getUsernameTextField()
        {
		if (usernameTextBox == null)
                {
                    usernameTextBox = new JTextField();
                    usernameTextBox.setPreferredSize(new Dimension(120, 20));
                    usernameTextBox.addFocusListener(new FocusTextFieldAdapter());
		}
		return usernameTextBox;
	}

	private JPasswordField getPasswordTextField()
        {
		if (passwordTextBox == null)
                {
                    passwordTextBox = new JPasswordField();
                    passwordTextBox.setPreferredSize(new Dimension(120, 20));
                    passwordTextBox.addFocusListener(new FocusTextFieldAdapter());
		}
		return passwordTextBox;
	}


	private String getPassword() {
		return new String(getPasswordTextField().getPassword());
	}

	private String getUsername() {
		return getUsernameTextField().getText();
	}

	private void error(Throwable t) {  
		t.printStackTrace();
		JOptionPane.showMessageDialog(this, t.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	}

	private void waitCursor() {
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	}

	private void defaultCursor() {
		setCursor(Cursor.getDefaultCursor());
	}

	protected void login()
        {
		if (client == null)
                {
			try
                        {

                        waitCursor();
                        client = new PicasawebClient(service, getUsername(), getPassword());
                        System.out.println("Mpikes!!!");
                        loginstate = true; //eisai syndedemenos!
                        getLoginButton().setText("Logoff");
                        getUsernameTextField().setEditable(false);
                        getPasswordTextField().setEditable(false);
			}
                        catch (Exception e)
                        {
			  error(e);
			}
                        finally
                        {
		         defaultCursor();
			}
		}
                else
                {
                    client = null;
                    getLoginButton().setText("Logon");
                    loginstate = false ;  //DEN eisai syndedemenos!!!
                    getUsernameTextField().setEditable(true);
                    getPasswordTextField().setEditable(true);
		}
	}





   public void SendImage() throws AuthenticationException, MalformedURLException, IOException, ServiceException
{

    //Tsekarw posa album exw:
/* 
   final PicasawebService myService = new PicasawebService("Picasa test");
   myService.setUserCredentials("elenalda03@gmail.com", "04160920"); 

 * DEN mou xreiazetai pleon afou to xw kanei MULTIUSER            */
 
                                                                    
   //posa album exw:
URL feedUrl = new URL("https://picasaweb.google.com/data/feed/api/user/default");

UserFeed myUserFeed = service.getFeed(feedUrl, UserFeed.class);

ArrayList<String> list = new ArrayList(); //lista onomatwn ALBUMS
 id = new ArrayList();  //lista twn IDs
ArrayList<String> id2 = new ArrayList();
int i=0;
String wtf = null;
/*
 *  Vazw ta onomata twn albums  se ena arraylist<String> 1 gia na gemisw to combo box kai
 *  2 gia to id tou album
 */
for (AlbumEntry  myAlbum : myUserFeed.getAlbumEntries()) {
    //System.out.println(myAlbum.getTitle().getPlainText());
    list.add(myAlbum.getTitle().getPlainText());//tittle
    id.add(myAlbum.getId()); //id
    //myAlbum.getFeedLink();
}
//System.out.println("this is wtf string:"+wtf);


//elegxos gia swsti metafora
for(i=0;i<id.size(); i++){
    System.out.println("IDs twn album:  "+id.get(i));
    }


String[] RESERVED_WORDS = new String[list.size()];

/*
 ftaksame ena arraylist k to metaferame se string gia combo box
 */

list.toArray(RESERVED_WORDS);
/*for(i=0;i<list.size(); i++){
        System.out.println(RESERVED_WORDS[i]);
        }*/

//end elegxos

//Kainourgio frame
    final JFrame f = new JFrame("Picasa Service");  //egine final logw hide on close sto telos!
    f.setSize(300,160);
    GridLayout gridLayout = new GridLayout();
    gridLayout.setRows(4);
    gridLayout.setColumns(2);
    JLabel jLabel1 = new JLabel();
    jLabel1.setText("Title ");
    JLabel jLabel2 = new JLabel();
    jLabel2.setText("Description  ");
    JLabel jLabel3 = new JLabel();
    jLabel3.setText("Available albums");
    JLabel jLabel4 = new JLabel();
    jLabel4.setText("Upload to Picasa");

    final JTextField text = new JTextField();
    final JTextArea textArea = new JTextArea();
    combo = new JComboBox(RESERVED_WORDS);
    //combo.setSize(2, 1);
    JButton buttonSend = new JButton("Upload!");
    JPanel jContentPane = new JPanel();
    jContentPane.setLayout(gridLayout);
    jContentPane.add(jLabel1);
    jContentPane.add(text);
    jContentPane.add(jLabel2);
    jContentPane.add(textArea);
    jContentPane.add(jLabel3);
    jContentPane.add(combo);
    jContentPane.add(jLabel4);
    jContentPane.add(buttonSend);
   // jContentPane.add(combo);
    f.add(jContentPane);
    //f.pack();
    f.setVisible(true);

// telos tou Kainourgiou frame

    //action listener gia combo box
     combo.addItemListener(new ItemListener()
     {
       public void itemStateChanged(ItemEvent ie)
       {
         str = (String)combo.getSelectedItem();
        //txt.setText(str);
         index=combo.getSelectedIndex();
         System.out.println("You have chosen: "+str);
         System.out.println("You have chosen No:"+index);
         String albumid=null;
         albumid = id.get(index);
         for(int i=albumid.length(); i>albumid.length()-19; i--)
         {
          albumid.getChars(albumid.length()-19, albumid.length(), charArray, 0);
          }

              for(int i=0; i<19; i++){
      // System.out.print("my char"+charArray[i]);
                }

    String MyId = new String(charArray);
    System.out.print("My user ID is: " + MyId);

 
    System.out.print("The Album ID is: "+albumid);
                try {
                   albumPostUrl = new URL("https://picasaweb.google.com/data/feed/api/user/default/albumid/"+MyId);//+MyId
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
                }


        //Construct photoEntry
     myPhoto = new PhotoEntry();
    String title = text.getText();
    String desc = textArea.getText();
    System.out.println("Title given: " +title);
    System.out.println("Description given: " +desc); 
    
    myPhoto.setTitle(new PlainTextConstruct(title));
    myPhoto.setDescription(new PlainTextConstruct(desc));
    myPhoto.setClient("PicasawebUIClient");



       // java.awt.Image image22 = ImageIO.read(null)
   MediaFileSource myMedia = new MediaFileSource(new File (path), "image/jpeg");
  // MediaFileSource myMedia = new MediaFileSource(new File(path), "image/jpeg");
   //MediaSource myMedia = new MediaSource(image, "image/jpeg");
   myPhoto.setMediaSource(myMedia);


      }
    }); // telos combobox

  // System.out.println(id.get(index));
    

//action listener gia to sendButton
   buttonSend.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent event) {
                               
                    try {

  PhotoEntry returnedPhoto = service.insert(albumPostUrl, myPhoto); //"https://picasaweb.google.com/data/feed/api/user/default/albumid/"
           f.hide();

           }

                  catch (IOException ex) {
            Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ImageProcessingFrame.class.getName()).log(Level.SEVERE, null, ex);
        }


           }
   }); //telos tou action listener gia to sendButton


   } //telos tis methodou SendImage();

   
}